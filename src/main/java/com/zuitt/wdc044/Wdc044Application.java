package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// "@SpringBootApplication" this is called as "Annotations" mark.
	// Annotations are used to provide supplemental information about the program.
	// These are used to manage and configure the behavior of the framework.
	// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

// To specify the main class of the Springboot application.

@SpringBootApplication
// Tells the Springboot that this will handle endpoints for web request.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}
	// This is used for mapping HTTP GET requests.
	@GetMapping("/hello")
	// "@RequestParam" is used to extract query parameters.
		// ex: name = john; Hello john
		// if no specified value: name: World; Hello World
		// To append the URL with a name parameter we do the following:
			// http:localhost:8080/hello?name=john
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}

	// ACTIVITY s01:

		@GetMapping("/hi")

		public String hi(@RequestParam(value = "user", defaultValue = "user") String user){
			return String.format("Hi %s!", user);
		}

		@GetMapping("/nameAge")
		public String nameAge(@RequestParam(value = "user", defaultValue = "name") String user, @RequestParam(value = "age", defaultValue = "age")  String age){
			return String.format("Hello %s! Your age is %s.", user, age);
		}

}
